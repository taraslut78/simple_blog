import logging

from django.http import HttpRequest
from rest_framework import viewsets

from blog.models import UserBlogger, Topic, Post
from blog.serialize import UserSerializer, PostSerializer, TopicSerializer

logger = logging.getLogger(__name__)


class UserViewSet(viewsets.ModelViewSet):
    queryset = UserBlogger.objects.all()
    serializer_class = UserSerializer


class TopicViewSet(viewsets.ModelViewSet):
    queryset = Topic.objects.all()
    serializer_class = TopicSerializer


class PostViewSet(viewsets.ModelViewSet):
    queryset = Post.objects.all()
    serializer_class = PostSerializer

    # noqa:C901
    def list(self, request: HttpRequest, *args, **kwargs):
        try:
            topic_id = request.GET.get('topic')
            if topic_id:
                self.queryset = self.queryset.filter(topic__id=int(topic_id))
        except Exception:
            logger.error(f"Parameter request.GET.get('topic') = {request.GET.get('topic')} is wrong")
        try:
            user_id = request.GET.get('user')
            if user_id:
                self.queryset = self.queryset.filter(user__id=int(user_id))
        except Exception:
            logger.error(f"Parameter request.GET.get('user') = {request.GET.get('user')} is wrong")

        return super(PostViewSet, self).list(self, request, *args, **kwargs)
