from django.db import models


class UserBlogger(models.Model):
    name = models.CharField(max_length=20, verbose_name="Ім'я")
    password = models.CharField(max_length=120, verbose_name="Пароль")

    def __str__(self):
        return f"{self.name}"


class Topic(models.Model):
    title = models.CharField(max_length=120, verbose_name="Розділ")

    def __str__(self):
        return f'{self.title}'


class Post(models.Model):
    title = models.CharField(max_length=250, verbose_name='Заголовок')
    text = models.CharField(max_length=1000, verbose_name="Текст")
    user = models.ForeignKey(UserBlogger, on_delete=models.DO_NOTHING)
    topic = models.ForeignKey(Topic, on_delete=models.CASCADE)

    def __str__(self):
        return f"Title: {self.title[:20]} ==> {self.text[:100]}"
