from django.urls import path, include
from rest_framework import routers

from blog.views import UserViewSet, TopicViewSet, PostViewSet

router = routers.DefaultRouter()
router.register('user', UserViewSet)
router.register('topic', TopicViewSet)
router.register('post', PostViewSet)

urlpatterns = [
    path('', include(router.urls))
]
