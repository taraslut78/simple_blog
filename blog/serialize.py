from rest_framework import serializers
from rest_framework.reverse import reverse

from .models import UserBlogger, Topic, Post


class UserSerializer(serializers.HyperlinkedModelSerializer):
    user_posts = serializers.SerializerMethodField()

    class Meta:
        model = UserBlogger
        fields = ['id', 'name', 'url', 'user_posts']

    def get_user_posts(self, obj):
        request = self.context['request']
        return reverse('post-list', request=request) + f'?user={obj.id}'


class TopicSerializer(serializers.HyperlinkedModelSerializer):
    topic_posts = serializers.SerializerMethodField()

    class Meta:
        model = Topic
        fields = "__all__"


class PostSerializer(serializers.HyperlinkedModelSerializer):
    topic_title = serializers.SerializerMethodField()
    user_name = serializers.SerializerMethodField()

    class Meta:
        model = Post
        fields = '__all__'

    @staticmethod
    def get_topic_title(obj):
        return obj.topic.title

    @staticmethod
    def get_user_name(obj):
        return obj.user.name
