from django.contrib import admin
from blog.models import UserBlogger, Topic, Post

admin.site.register(UserBlogger)
admin.site.register(Topic)
admin.site.register(Post)
