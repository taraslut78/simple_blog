FROM python:3.8

WORKDIR /usr/srs/app
COPY requirement.txt ./
RUN pip install -r requirement.txt
COPY . .
RUN python manage.py makemigrations
RUN python manage.py migrate

#EXPOSE 8000
#CMD ["python", "manage.py", "runserver", "0.0.0.0:8000"]
